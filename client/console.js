/**'
 * Node is for networks.  No browser required. 
 * This is a simple console app to test our server functions.
 * 
 * All await calls must be wrapped in an sync function
 */
const temperatureService = require('../server/TemperatureService.js')
const weatherService = require('../server/WeatherService.js')

async function getTemp(city) {
    const ans = await temperatureService.get(city)
}

async function getWeatherTemp(city) {
    const temp = weatherService.getTemp(city)
}

async function getWeatherHumidity(city) {
    const humidity = weatherService.getHumidity(city)
}

getTemp('Paris, France')
getTemp('Maryville, Missouri')
getTemp('AUstin, Texas')
getWeatherTemp('Hyderabad, India')
getWeatherHumidity('Wichita, Kansas')

// in what order will our functions return?
